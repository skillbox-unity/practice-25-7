using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondChallenge : MonoBehaviour
{
    public void ChangeTransform()
    {
        Vector3 scaleChange = new Vector3(1.2f, 1.2f, 1.2f);
        transform.localScale += scaleChange;
    }

    public void SetForce()
    {
        Rigidbody rigid = GetComponent<Rigidbody>();
        float force = 8.0f;
        rigid.AddForce(Vector3.up * force, ForceMode.Impulse);
    }

    public void DestroyCube()
    {
        Destroy(gameObject);
    }
}
